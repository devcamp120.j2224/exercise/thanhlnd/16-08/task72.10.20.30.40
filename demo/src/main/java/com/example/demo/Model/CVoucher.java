package com.example.demo.Model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;

@Entity // khai báo entity để nó tạo ra 1 bảng
@Table(name = "pizza_vouchers")
public class CVoucher {
    // khai báo 1 key iD 
    @Id 
    @GeneratedValue (strategy =  GenerationType.AUTO)
    private long id ;

    // sửa lại giá trị cho cột
    @Column(name = "phan_tram_gia_gia")
    private String phanTramGiamGia ;

    @Column(name = "ghi_chu")
    private String ghiChu ;

    @Column(name = "ma_voucher")
    private String maVoucher ;

    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @Column(name = "ngay_tao")
    private Date ngayTao ;

    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedBy
    @Column(name = "ngay_cap_nhat")
    private Date ngayCapNhat;


    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getPhanTramGiamGia() {
        return phanTramGiamGia;
    }
    public void setPhanTramGiamGia(String phanTramGiamGia) {
        this.phanTramGiamGia = phanTramGiamGia;
    }
    public String getGhiChu() {
        return ghiChu;
    }
    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }
    public String getMaVoucher() {
        return maVoucher;
    }
    public void setMaVoucher(String maVoucher) {
        this.maVoucher = maVoucher;
    }
    public Date getNgayTao() {
        return ngayTao;
    }
    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }
    public Date getNgayCapNhat() {
        return ngayCapNhat;
    }
    public void setNgayCapNhat(Date ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }
    public CVoucher() {

    }
    public CVoucher(long id, String phanTramGiamGia, String ghiChu, String maVoucher, Date ngayTao, Date ngayCapNhat) {
        this.id = id;
        this.phanTramGiamGia = phanTramGiamGia;
        this.ghiChu = ghiChu;
        this.maVoucher = maVoucher;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }

    
}
