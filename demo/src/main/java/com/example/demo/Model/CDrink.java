package com.example.demo.Model;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "pizza_drinks")
public class CDrink {
    @Id
    @GeneratedValue (strategy =  GenerationType.AUTO)
    private long id ;

    @Column(name = "ma_nuoc_uong")
    private String maNuocUong ;

    @Column(name = "ten_nuoc_uong")
    private String tenNuocUong ; 

    @Column(name = "price")
    private long price ;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ngay_tao" , nullable = true , updatable = false)
    @CreatedDate
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private Date ngayTao ;


    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ngay_cap_nhat" , nullable = true )
    @LastModifiedDate // ngày cập nhật cuối 
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private Date ngayCapNhat;

    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMaNuocUong() {
        return maNuocUong;
    }

    public void setMaNuocUong(String maNuocUong) {
        this.maNuocUong = maNuocUong;
    }

    public String getTenNuocUong() {
        return tenNuocUong;
    }

    public void setTenNuocUong(String tenNuocUong) {
        this.tenNuocUong = tenNuocUong;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public Date getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }

    public Date getNgayCapNhat() {
        return ngayCapNhat;
    }

    public void setNgayCapNhat(Date ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    public CDrink() {

    }

    public CDrink(long id, String maNuocUong, String tenNuocUong, long price, Date ngayTao, Date ngayCapNhat) {
        this.id = id;
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
        this.price = price;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }

}

