package com.example.demo.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.CUser;

public interface iUserRespository extends JpaRepository <CUser , Long> {
    CUser findById(long id);
}
