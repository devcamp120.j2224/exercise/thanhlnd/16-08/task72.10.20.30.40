package com.example.demo.Respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.CRegion;

public interface iRegionRespository extends JpaRepository <CRegion , Long> {
    List<CRegion> findByCountryId(Long countryId);
    boolean existsRegionsByRegionCode(String countryCode);
}
