package com.example.demo.Controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.CDrink;
import com.example.demo.Respository.iDrinkRespository;

@RestController
@CrossOrigin
@RequestMapping("drink")
public class CDrinkController {
    
    @Autowired
    iDrinkRespository iDrinkRespository ;


    // lấy danh sách all drink
    @GetMapping("/all")
    public ResponseEntity <List <CDrink>> getDrinks() {
        
        try {
            List<CDrink> listDrink = new ArrayList<CDrink>();
            iDrinkRespository.findAll().forEach(listDrink :: add);

            if(listDrink.size() == 0){
                return new ResponseEntity<List <CDrink>>(listDrink, HttpStatus.NOT_FOUND);
            }
            else{
                return new ResponseEntity<List <CDrink>>(listDrink, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // tìm drink bởi id
    @GetMapping("/detail")
    public ResponseEntity<Object> getuserById(@RequestParam(name = "id", required = true) Long id) {
        Optional<CDrink> userFounded = iDrinkRespository.findById(id);
        if (userFounded.isPresent())
            return new ResponseEntity<Object>(userFounded, HttpStatus.OK);
        else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

    // tạo drink 
    @PostMapping("/create")
    public ResponseEntity<Object> createDrink(@RequestBody CDrink cDrink) {
        Optional<CDrink> cDrinkData = iDrinkRespository.findById(cDrink.getId());
        try {
        if(cDrinkData.isPresent()) {
            return ResponseEntity.unprocessableEntity().body(" Drink already exsit  ");
        }
        cDrink.setMaNuocUong(cDrink.getMaNuocUong());
        cDrink.setTenNuocUong(cDrink.getTenNuocUong());
        cDrink.setPrice(cDrink.getPrice());
        cDrink.setNgayTao(new Date());
        cDrink.setNgayCapNhat(null);

    
        CDrink saveDrink = iDrinkRespository.save(cDrink);
        return new ResponseEntity<>(saveDrink, HttpStatus.CREATED);
    }
        catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 
    
    // update drink bởi id
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateDrinkById(@PathVariable (name = "id")Long id , @RequestBody CDrink cDrink) {
        Optional<CDrink> cDrinkData = iDrinkRespository.findById(id);
        if (cDrinkData.isPresent()) {
			CDrink saveDrink = cDrinkData.get();

		    saveDrink.setMaNuocUong(cDrink.getMaNuocUong());
            saveDrink.setTenNuocUong(cDrink.getTenNuocUong());
            saveDrink.setPrice(cDrink.getPrice());
            saveDrink.setNgayCapNhat(new Date());
            try {
                return ResponseEntity.ok(iDrinkRespository.save(saveDrink));
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                    .body("Can not execute operation of this Entity"+ e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<Object> (null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // delete drink bởi id
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<CDrink> deleteDrinkById(@PathVariable (name = "id") Long id) {
        try {
			iDrinkRespository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }

}
