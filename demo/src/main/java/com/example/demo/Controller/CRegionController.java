package com.example.demo.Controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.CCountry;
import com.example.demo.Model.CRegion;
import com.example.demo.Respository.iCountryRespository;
import com.example.demo.Respository.iRegionRespository;

@RestController
@CrossOrigin
@RequestMapping("region")
public class CRegionController {
	@Autowired
	private iRegionRespository iregionRepository;
	
	@Autowired
	private iCountryRespository icountryRepository;
	

    // tạo region bởi id của country
    @PostMapping("/create/{id}")
    public ResponseEntity<Object> createRegion(@PathVariable (name = "id") Long id , @RequestBody CRegion cRegion) {
       
			Optional<CCountry> countryData = icountryRepository.findById(id);
			if (countryData.isPresent()) {
			CRegion newRole = new CRegion();
			newRole.setRegionName(cRegion.getRegionName());
			newRole.setRegionCode(cRegion.getRegionCode());
			newRole.setCountry(cRegion.getCountry());
			
			CCountry _country = countryData.get();
			newRole.setCountry(_country);
			newRole.setCountryName(_country.getCountryName());

			if(iregionRepository.existsRegionsByRegionCode(cRegion.getRegionCode())){

				return new ResponseEntity<Object>(  "timestamp : " + new Date()         	+ "\n"  + "\n" +
													"errors: [ "                        	+ "\n"  + "\n" +  
																"regionCode đã tồn tại"   	+ "\n"  + "\n" + 
															"]"                         	+ "\n"  + "\n" + 
													"status: 409 " , HttpStatus.CONFLICT);

			 }

			try {
			
			CRegion savedRole = iregionRepository.save(newRole);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
			}
		 catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body(" "+e.getCause().getCause().getMessage());
		}
	}	
			else{
				return new ResponseEntity<>(new Date() + "\n" +
				" : không tìm thấy id của country  " + "\n" +
				" : có thể server đang có vấn đề , thử lại trong giây lát " + "\n" +
				" : trong lúc chờ vui lòng xem lại quá trình tạo mới region của bạn ", HttpStatus.NOT_FOUND);
			}
    	}

	// update region bởi id region
	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updateRegion(@PathVariable("id") Long id, @RequestBody CRegion cRegion) {

		Optional<CRegion> region = iregionRepository.findById(id);

		if(region.isPresent()){	
			CRegion newRegion = region.get();
			newRegion.setRegionName(cRegion.getRegionName());
			newRegion.setRegionCode(cRegion.getRegionCode());

			return new ResponseEntity<>(iregionRepository.save(newRegion), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}

	// xóa region by id
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deleteRegionById(@PathVariable Long id) {
		try {
			iregionRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// tìm region bằng id
	@GetMapping("/details/{id}")
	public CRegion getRegionById(@PathVariable Long id) {
		if (iregionRepository.findById(id).isPresent())
			return iregionRepository.findById(id).get();
		else
			return null;
	}

    // lấy all region
	@GetMapping("/all")
    public ResponseEntity<Object> getAllorders(){
        List<CRegion> regionList = new ArrayList<CRegion>();
        try {
            iregionRepository.findAll().forEach(orderElement -> {
                regionList.add (orderElement);
            });
            return new ResponseEntity<Object>(regionList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }
	// // tìm region bởi countryid 
	@GetMapping("/{countryId}/regions")
    public List < CRegion > getRegionsByCountryId(@PathVariable(value = "countryId") Long countryId) {
        return iregionRepository.findByCountryId(countryId);
    }
	

    // đếm country 
    @GetMapping("/{countryId}/count")
    public int countRegionByCountryId(@PathVariable(value = "countryId") Long countryId) {
        return iregionRepository.findByCountryId(countryId).size();
    }

    // check region bởi id 
    @GetMapping("/check/{id}")
	public boolean checkRegionById(@PathVariable Long id) {
		return iregionRepository.existsById(id);
	}		
}

