package com.example.demo.Controller;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import com.example.demo.Model.COrder;
import com.example.demo.Model.CUser;
import com.example.demo.Respository.iOrderRespository;
import com.example.demo.Respository.iUserRespository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("order")
public class COrderController {
    @Autowired
    iOrderRespository iOrderRepository;

    @Autowired
    iUserRespository iUserRepository;

    // lấy danh sách all order
    @GetMapping(path = "/all")
    public ResponseEntity<Object> getAllorders(){
        List<COrder> orderList = new ArrayList<COrder>();
        try {
           
            iOrderRepository.findAll().forEach(orderElement -> {
                orderList.add (orderElement);
            });
            return new ResponseEntity<Object>(orderList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }


    // tìm order bởi userId
    @GetMapping("/search/userId={id}")
    public ResponseEntity<Set<COrder>> getOrdersByUserId(@PathVariable long id){
        try {
            
            CUser user = iUserRepository.findById(id);
            if(user != null){
                return new ResponseEntity<Set<COrder>>(user.getOrders(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // tìm order bởi orderId
    @GetMapping("/detail/{id}")
    public ResponseEntity<Object> getOrderById(@PathVariable (name = "id") Long id) {
        Optional<COrder> order = iOrderRepository.findById(id) ;
        if(order.isPresent()) {
            return new ResponseEntity<Object>(order, HttpStatus.OK);
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

    // tạo order by userId
    @PostMapping("/create/{id}")
    public ResponseEntity<Object> create0rder(@PathVariable (name = "id") Long id , @RequestBody COrder newOrder) {
        Optional<CUser> cUserData = iUserRepository.findById(id);
        Optional<COrder> cOrderFound = iOrderRepository.findById(newOrder.getId());
        if(cUserData.isPresent()){
            try {
                if(cOrderFound.isPresent()){
                    return ResponseEntity.unprocessableEntity().body(" order already exsit  ");
                }
        
        CUser cUserNew = cUserData.get();
        newOrder.setcUser(cUserNew);
        newOrder.setCreated(new Date());
        
        COrder order = iOrderRepository.save(newOrder);

            return new ResponseEntity<Object>(order, HttpStatus.CREATED);
        } 
        catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                .body("Can not execute operation about this Entity " +e.getCause().getMessage());
        }
    }    
        else {
                 return ResponseEntity.badRequest().body("Failed to get specified id: " + id);
             }
    }
 

    // update order by orderId
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateOrder(@PathVariable (name = "id") Long id, @RequestBody COrder orderUpdate) {
        Optional<COrder> orderData = iOrderRepository.findById(id);
        try {
        if (orderData.isPresent()) {
            COrder order = orderData.get();

            order.setUpdated(new Date()) ;
            order.setPizzaSize(orderUpdate.getPizzaSize());
            order.setPizzaType(orderUpdate.getPizzaType());
            order.setPrice(orderUpdate.getPrice());
            order.setPaid(orderUpdate.getPaid());
            order.setVoucherCode(orderUpdate.getVoucherCode());
            order.setUpdated(new Date());
           
                return new ResponseEntity<>(iOrderRepository.save(order) , HttpStatus.OK);
            }
        else {
            return new ResponseEntity<Object>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }         
            catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                .body("Can not execute operation of this Entity" + e.getCause().getCause().getMessage());
            }
       
    }

    // delete order bởi id
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteOrderById(@PathVariable Long id) {
        try {
            iOrderRepository.deleteById(id) ;
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
            .body("Can not execute operation of this Entity"+ e.getCause().getCause().getMessage());
        }
    }

        // đếm order
        @GetMapping("/{userId}/count")
        public int countOrderByUserId(@PathVariable(value = "userId") Long userId) {
            return iOrderRepository.findByUserId(userId).size();
        }
    
        // check order bởi id 
        @GetMapping("/check/{id}")
        public boolean checkOrderById(@PathVariable Long id) {
            return iOrderRepository.existsById(id);
        }		
    }


